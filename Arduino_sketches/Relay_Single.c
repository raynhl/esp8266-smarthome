#include <FS.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <WiFiManager.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ArduinoJson.h>
#include <string.h>

#define FW_MAJOR_VERSION				0x02
#define FW_MINOR_VERSION				0x01

struct FotaHeader {
	uint32_t	vendorId;
	uint32_t	deviceId;
	uint8_t		majorVersion;
	uint8_t		minorVersion;
};

// Function declarations - start
String 		getServerHeartBeatUrl();
void 		handleUpdateStatus();
void 		handleFotaUpdate();
void 		checkForUpdates();
bool 		matchVendorIdAndDeviceId(FotaHeader* fotaHeader);
bool 		needFwUpdate(FotaHeader* fotaHeader);
void 		performUpdate();
void 		sendHeartBeat();

void 		saveConfigCallback();
void 		readFromConfigFile();
void 		saveToConfigFile();
void 		updateValues();
String 		getApName();
String 		getApPassword();
void 		setUpWifi();

void 		printFwVersion();
uint32_t 	toHex(char* array);
void 		turnOnRelay();
void 		turnOffRelay();
// Function declarations - end


WiFiManagerParameter* customServerBaseUrl;
WiFiManagerParameter* customVendorId;
WiFiManagerParameter* customDeviceId;
ESP8266WebServer* server;


// Set up buffers to contain values from config.json and for Setup Page - start
char 			vendorId[5] 				= "NULL";
char 			deviceId[5] 				= "NULL";
char 			serverBaseUrlBuffer[50];
char 			staticIp[16];
char 			staticGw[16];
char 			staticSn[16];
// Set up buffers to contain values from config.json and for Setup Page - end


String 			serverBaseUrl 				= "http://<server_ip_addr>:<port>/";
String 			serverHeartbeatUrl 			= getServerHeartBeatUrl();
bool 			shouldSaveConfig 			= false;
unsigned long 	currentMillis 				= 0;
unsigned long 	previousMillis 				= 0;
long 			heartBeatInterval 			= 10000;
uint8_t 		currentState 				= 0xFF;


String getServerHeartBeatUrl() {
	String heartbeatUrl = serverBaseUrl;
	heartbeatUrl.concat("heartbeat/");
	heartbeatUrl.concat(String(vendorId));
	heartbeatUrl.concat("/");
	heartbeatUrl.concat(String(deviceId));
	heartbeatUrl.concat("/");
	return heartbeatUrl;
} 

void handleUpdateStatus() {
	Serial.println("Updating Status");

	StaticJsonBuffer<150> JSONbuffer;
	JsonObject& parsed = JSONbuffer.parseObject(server->arg(0));
	
	if (!parsed.success()) {
		Serial.println("Parsing failed");
		server->send(404, "text/plain", "");
	}

	const char *relayStatusPtr = parsed["status"];
	int relayStatus = String(relayStatusPtr).toInt();
	Serial.print("Status: ");
	Serial.println(relayStatus);

	if (relayStatus) {
		turnOnRelay();
	}

	else {
		turnOffRelay();
	}

	server->send(200, "text/plain", "");
}

void handleFotaUpdate() {
	server->send(200, "text/plain", "");
	checkForUpdates();
}

void checkForUpdates() {
	String fwVersionURL = String(serverBaseUrl);
	fwVersionURL.concat("fota/");
	fwVersionURL.concat(String(vendorId));
	fwVersionURL.concat("/");
	fwVersionURL.concat(String(deviceId));
	fwVersionURL.concat("/check");
	Serial.println(fwVersionURL);

	HTTPClient httpClient;
	httpClient.begin(fwVersionURL);

	if (httpClient.GET() == 200) {
		String payload = httpClient.getString();
		byte payloadBuffer[sizeof(FotaHeader)];
		payload.getBytes(payloadBuffer, sizeof(FotaHeader));
		struct FotaHeader* fotaHeader = (FotaHeader*) &payloadBuffer;

		if (matchVendorIdAndDeviceId(fotaHeader) && needFwUpdate(fotaHeader)) {
			Serial.println("New FW version available. Starting update.");
			performUpdate();
		}

		if (matchVendorIdAndDeviceId(fotaHeader)) {
			Serial.println("Already running on latest FW version."); 
		}

		else {
			Serial.println("Vendor ID and/or Device ID mismatch. Check if firmware is for this device.");
		}
	}

	httpClient.end();
}

bool matchVendorIdAndDeviceId(FotaHeader* fotaHeader) {
	return (fotaHeader->vendorId == toHex(vendorId)) && (fotaHeader->deviceId == toHex(deviceId));
}

bool needFwUpdate(FotaHeader* fotaHeader) {
	return (fotaHeader->minorVersion > FW_MINOR_VERSION) || (fotaHeader->majorVersion > FW_MAJOR_VERSION);
}

void performUpdate() {
	String fwImageURL = String(serverBaseUrl);
	fwImageURL.concat("fota/");
	fwImageURL.concat(String(vendorId));
	fwImageURL.concat("/");
	fwImageURL.concat(String(deviceId));
	fwImageURL.concat("/image");
	Serial.println(fwImageURL);

	// shutdown device function
	turnOffRelay();

	t_httpUpdate_return ret = ESPhttpUpdate.update(fwImageURL);

	switch(ret) {
		case HTTP_UPDATE_FAILED:
		Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
		break;

		case HTTP_UPDATE_NO_UPDATES:
		Serial.println("HTTP_UPDATE_NO_UPDATES");
		break;
	}
}

void sendHeartBeat() {
	String heartBeatURL = String(serverHeartbeatUrl);
	heartBeatURL.concat(String(FW_MAJOR_VERSION,HEX));
	heartBeatURL.concat("/");
	heartBeatURL.concat(String(FW_MINOR_VERSION,HEX));
	heartBeatURL.concat("/");
	heartBeatURL.concat(String(currentState,HEX));
	Serial.println(heartBeatURL);
  
	HTTPClient httpClient;
	httpClient.begin(heartBeatURL);

	if (httpClient.GET() == 200) {
		Serial.println("Heartbeat update successful.");
	}

	else {
		Serial.println("Heartbeat update failed.");
	}
}

void saveConfigCallback() {
  shouldSaveConfig = true;
}

void readFromConfigFile() {
	Serial.println("[CONFIG] Mounting FS.");

	if (SPIFFS.begin()) {
		Serial.println("[CONFIG] Mounted FS.");
		
		if (SPIFFS.exists("/config.json")) {
			//file exists, reading and loading
			Serial.println("[CONFIG] Reading config.json from SPI");
			File configFile = SPIFFS.open("/config.json", "r");
			
			if (configFile) {
				Serial.println("[CONFIG] Opened config.json file");
				size_t size = configFile.size();
				
				// Allocate a buffer to store contents of the file.
				std::unique_ptr<char[]> buf(new char[size]);

				configFile.readBytes(buf.get(), size);
				DynamicJsonBuffer jsonBuffer;
				JsonObject& json = jsonBuffer.parseObject(buf.get());
				json.printTo(Serial);
				Serial.println();

				if (json.success()) {
					Serial.println("[CONFIG] Parsed config.json");

					// Populate buffers with values from config.json
					strcpy(serverBaseUrlBuffer, json["server_base_url"]);
					strcpy(vendorId, json["vendor_id"]);
					strcpy(deviceId, json["device_id"]);

					serverBaseUrl = String(serverBaseUrlBuffer);
					serverHeartbeatUrl = getServerHeartBeatUrl();

					if(json["ip"]) {
						Serial.println("[CONFIG] Setting custom IP from config.json");

						strcpy(staticIp, json["ip"]);
						strcpy(staticGw, json["gateway"]);
						strcpy(staticSn, json["subnet"]);
					} 

					else {
						Serial.println("[CONFIG] No custom IP in config.json");
					}
		
				} 

				else {
					Serial.println("[CONFIG] Failed to load json config.json");
				}
			}
		}
	} 

	else {
		Serial.println("[ERROR] Failed to mount FS");
	}
}

void saveToConfigFile() {
	Serial.println("[CONFIG] Saving values to config.json file.");
	DynamicJsonBuffer jsonBuffer;
	JsonObject& json = jsonBuffer.createObject();
	json["server_base_url"] = serverBaseUrl;
	json["vendor_id"] = String(vendorId);
	json["device_id"] = String(deviceId);
	json["ip"] = WiFi.localIP().toString();
	json["gateway"] = WiFi.gatewayIP().toString();
	json["subnet"] = WiFi.subnetMask().toString();

	File configFile = SPIFFS.open("/config.json", "w");
	if (!configFile) {
		Serial.println("[CONFIG] Failed to open config.json for writing.");
		return;
	}

	json.printTo(configFile);
	configFile.close();
}

void updateValues() {
	strcpy(serverBaseUrlBuffer, customServerBaseUrl->getValue());
	strcpy(vendorId, customVendorId->getValue());
	strcpy(deviceId, customDeviceId->getValue());
	serverBaseUrl = String(serverBaseUrlBuffer);
	serverHeartbeatUrl = getServerHeartBeatUrl();
}

String getApName() {
	String ApName = "ESP:";
	ApName.concat(String(vendorId));
	ApName.concat(String(deviceId));
	return ApName;
}

String getApPassword() {
	String password;
	password.concat(String(vendorId));
	password.concat(String(deviceId));
	return password;
}

void setUpWifi() {
	WiFiManager wifiManager;

	customServerBaseUrl = new WiFiManagerParameter("server_base_url", "SERVER_BASE_URL", serverBaseUrlBuffer, 50);
	customVendorId = new WiFiManagerParameter("vendor_id", "VENDOR_ID", vendorId, 5);
	customDeviceId = new WiFiManagerParameter("device_id", "DEVICE_ID", deviceId, 5);
	
	wifiManager.addParameter(customServerBaseUrl);
	wifiManager.addParameter(customVendorId);
	wifiManager.addParameter(customDeviceId);

	// For testing. Force into AP mode
	// wifiManager.resetSettings();

	//set config save notify callback
	wifiManager.setSaveConfigCallback(saveConfigCallback);

	//set static ip
	IPAddress _ip,_gw,_sn;
	_ip.fromString(staticIp);
	_gw.fromString(staticGw);
	_sn.fromString(staticSn);

	wifiManager.setSTAStaticIPConfig(_ip, _gw, _sn);

	String apName = getApName();
	String apPassword = getApPassword();

	if (!wifiManager.autoConnect(apName.c_str(), apPassword.c_str())) {
		Serial.println("[ERROR] Failed to connect and hit timeout.");
		delay(3000);
		//reset and try again, or maybe put it to deep sleep
		ESP.reset();
		delay(5000);
	}
}

void printFwVersion() {
	Serial.print("Running on FW v");
	Serial.print(FW_MAJOR_VERSION, HEX);
	Serial.print(".");
	Serial.println(FW_MINOR_VERSION, HEX);
}

uint32_t toHex(char* array) {
	uint32_t hexValue;
	hexValue = (uint32_t) array[0] << 24 | (uint32_t) array[1] << 16 | (uint32_t) array[2] << 8 | (uint32_t) array[3];
	Serial.print("[HEX] ");
	Serial.println(hexValue, HEX);
	return hexValue;
}

void turnOnRelay() {
	const byte miBufferON[] = {0xA0, 0x01, 0x01, 0xA2};
	Serial.write(miBufferON, sizeof(miBufferON));
	currentState = 0x1;
}

void turnOffRelay() {
	const byte miBufferOFF[] = {0xA0, 0x01, 0x00, 0xA1};
	Serial.write(miBufferOFF, sizeof(miBufferOFF));
	currentState = 0x0;
}

void setup() {
	Serial.begin(9600);
	delay(100);

	readFromConfigFile();
	setUpWifi();

	if (shouldSaveConfig) {
		updateValues();
		saveToConfigFile();
	}

	server = new ESP8266WebServer(80);

	server->on("/update", handleUpdateStatus);
	server->on("/fota_update", handleFotaUpdate);
	server->begin();
	Serial.println("Server listening");
	printFwVersion();
}

void loop() {
	server->handleClient();
	currentMillis = millis();

	if (currentMillis - previousMillis >= heartBeatInterval) {
		previousMillis = currentMillis;
		sendHeartBeat();
	}
}



