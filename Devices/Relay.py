import requests
import time

class Relay:
	def __init__(self, device_config):
		self.vendor_id = device_config["vendor_id"]
		self.device_id = device_config["device_id"]
		self.name = device_config["device_name"]
		self.ip_address = device_config["ip_address"]
		self.major_version = device_config["major_version"]
		self.minor_version = device_config["minor_version"]
		self.multi_function = device_config["multi_function"]

		if self.multi_function:
			self.sub_devices_name = device_config["sub_devices"]

		else: self.sub_devices_name = []
		
		self.current_state = 0x00

		
	def update(self, data):
		url = "http://{}:80/update".format(self.ip_address)

		status = data["status"]

		if self.multi_function:
			index = self.sub_devices_name.index(data["sub_device"])
			status = self.current_state ^ (-data["status"] ^ self.current_state) & (1 << index) 

		try:
			r = requests.post(url, json={"status": status})
		except:
			print("[ERROR] Unable to make request to: {} ({}): {} | Check if device is offline?".format(self.name, self.device_id, self.ip_address))
			return 404

		self.current_state = status
		return r.status_code

	def delayed_update(self, data):
		delay = int(data["delay"]) * 60 # delay is minutes. Multiply by 60 to get seconds.

		print("[INFO] Thread started. Will perform update on {} ({}) after {} seconds.".format(self.name, self.device_id, delay))
		time.sleep(delay)	
		self.update(data)

	def timed_update(self, data):
		self.update(data)
		
		# update the opposite status after a certain period
		data["status"] ^= 1
		self.delayed_update(data)

	def print(self):
		print("------------------------------------")
		print("Vendor ID: {}".format(self.vendor_id))
		print("Device ID: {}".format(self.device_id))
		print("Device Name: {}".format(self.name))
		print("Device IP: {}".format(self.ip_address))
		print("Multi-function: {}".format(self.multi_function))
		print("Sub-Devices: {}".format(self.sub_devices_name))
		print("Current State: 0x{}".format(format(self.current_state, "02X")))
		print("Major Version: 0x{}".format(self.major_version))
		print("Minor Version: 0x{}".format(self.minor_version))
		print("------------------------------------")
		print("")
